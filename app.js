var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var methodOverride = require('method-override');
var i18n = require("i18n");

var expressWinston = require('express-winston');
var winston = require('winston'); // for transports.Console

var helmet = require('helmet');

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

const expressSwagger = require('express-swagger-generator')(app);

i18n.configure({
    locales:['es', 'en'],
    directory: __dirname + '/locales',
    defaultLocale: 'es',
});

app.use(i18n.init);
app.use(helmet());
app.use(compression());
app.use(methodOverride());

let options = {
    swaggerDefinition: {
        info: {
            description: 'Rest Api Server',
            title: 'Rest Api',
            version: '1.0.0',
        },
        host: 'localhost:3000',
        basePath: '/v1',
        produces: [
            "application/json",
            "application/xml"
        ],
        schemes: ['http', 'https'],
		securityDefinitions: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'Authorization',
                description: "",
            }
        }
    },
    basedir: __dirname, //app absolute path
    files: ['./routes/**/*.js'] //Path to the API handle folder
};
expressSwagger(options)

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({limit:'10mb'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// express-winston logger makes sense BEFORE the router
app.use(expressWinston.logger({
    transports: [
        new winston.transports.Console({
            colorize: true,
            timestamp: true,
        })
    ],
    meta:false,
    colorize: true,
    format: winston.format.combine(
        winston.format.colorize({ all: true }),
        winston.format.simple()
    )
}));

app.use('/', index);
app.use('/v1/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

module.exports = app;
