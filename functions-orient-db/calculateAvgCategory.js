/**/
var db = orient.getDatabase();
try {
  var query = db.query("select avg(preference) as avg, category from ( select preference, product, product.corridors.id as category from Preference where user = " + user + " unwind category) group by category");

  var rank = [];
  for(var i=0;i<query.length;i++) {
      rank.push({
          avg : query[i].field("avg"),
          category : query[i].field("category")
      });
  }

  return db.command("UPDATE" + user + " SET rank =" + JSON.stringify(rank));
} catch ( err ){
  return {error: err.toString(), code: 400};
}