var size = 4, products = [];
try {
  if(params){
      if(params.size){
         size = params.size;
      }
  }
  
  query = orient.getDatabase().query("SELECT *, math_random() as rand FROM Products order by rand limit " + size);
    
  if(query){
  	for(var i=0;i<query.length;i++) {
  	  var image = query[i].field("image");
      var path = "http://img.dev.rappi.com/products/";
      products.push({
        rid : query[i].field("@rid"),
        product_id : query[i].field("product_id"),
        name : query[i].field("name"),
        description : query[i].field("description"),
        price : query[i].field("price"),
        image : path + "low/" + image
      });
      
    }
  }
  
  return products;

} catch ( err ){
  return {error: err.toString(), code: 400};
}