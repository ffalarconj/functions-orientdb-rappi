var id, product;
try {
  if(params){
      if(params.id){
         id = params.id
      }else{
      	return {error: "id is mandatory", code: 400};
      }
  }
  
  query = orient.getDatabase().query("SELECT FROM Products WHERE product_id="+ id);
    
  if(query){
  	for(var i=0;i<query.length;i++) {
  	  var image = query[i].field("image");
      var path = "http://img.dev.rappi.com/products/";
      product = {
        product_id : query[i].field("product_id"),
        store_id : query[i].field("store_id"),
        name : query[i].field("name"),
        description : query[i].field("description"),
        price : query[i].field("price"),
        image : {
        	'hight' : path + "hight/" + image,
          	'medium' : path + "medium/" + image,
            'low' : path + "low/" + image,
        },
      };
      
    }
  }
  
  return product;

} catch ( err ){
  return {error: err.toString(), code: 400};
}