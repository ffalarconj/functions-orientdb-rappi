try {
    var limit = 10;
    var where = "";
    if(!user){
        return {error: "user is mandatory", code: 400};
    }
    if(size){
        limit =parseInt(size);
    }
    
    if(options){
       if(options.swipe){
           where = " where view is null";
       }
       if(options.favourite){
           where = " where view = true and avg >=0";
       }
    }
    
    var sql = "select from (select max(product) as product, max(avg) as avg, " + 
        "max(view) as view, max(category) as category, random() as rand from (select expand($u) " +
        " LET $a = (select product, preference as avg, product.corridors.id as category" + 
        ", true as view from ( select preference, product, product.corridors.id as category " +
        " from Preference where user = "+ user +")), " +
        " $b = (select product, avg, category from ( " +
          " select @rid as product, calculate(corridors.id, $categories.rank[0]) as avg, " +
          " corridors.id as category from products LET $categories = (select rank from "+ user +") " + 
        " unwind category )), $u = unionall($a,$b)) group by product )" + where +
        " order by view desc, rand limit " + limit;
    
    //return sql;
    
    var query = orient.getDatabase().query(sql);
    var products = [];
    var path = "http://img.dev.rappi.com/products/";
    for(var i=0;i<query.length;i++) {
          var image = query[i].field("product").field("image");
          products.push({
            rid : query[i].field("product"),
            product_id : query[i].field("product").field("product_id"),
            name : query[i].field("product").field("name"),
            description : query[i].field("product").field("description"),
            price : query[i].field("product").field("price"),
            view : query[i].field("view"),
            avg : query[i].field("avg"),
            image : path + "low/" + image
          });
    }
    return products;
  } catch ( err ){
    return {error: err.toString(), code: 400};
  }