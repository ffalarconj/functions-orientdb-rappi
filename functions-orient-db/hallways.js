try {
    var limit = 10;
    var where = "";
    if(!user){
        return {error: "user is mandatory", code: 400};
    }
    if(size){
        limit =parseInt(size);
    }
    
    var queryFavourite = orient.getDatabase().query("select predictions("+ user +", 20, { favourite : true}) as f");
      
    var sql = "select list(map('name', product.name, 'image', product.image, 'price', product.price, 'rid', product,'description', product.description, 'product_id', product.product_id )) as products, category, avg from (" + 
        "select max(product) as product, avg(avg) as avg, max(category) as category " +
        "from (select product, avg, category from (" +
        "select @rid as product, calculate(corridors.id, $categories.rank[0]) as avg" + 
        ", corridors.name as category, corridors.id as categoryId from products " + 
        " LET $categories = (select rank from "+ user +") unwind category, categoryId" +
           " )  where categoryId in (select category from (select avg(preference) as avg" + 
        ", category from ( select preference, product, product.corridors.id as category" + 
        " from Preference where user = "+ user +" unwind category) group by category ) " + 
        "order by avg desc limit 10)" +
          ") group by product ) where avg >=0 group by category " +
        "order by avg desc, category  limit " + limit;
    
    //return sql;
    
    var query = orient.getDatabase().query(sql);
    
    var categories = [];
    var pathImageCategory = "https://picsum.photos/400/100/?random&blur";
    categories.push({
        category : "Favoritos",
      avg : 2,
      products : queryFavourite[0].field('f'),
      image: pathImageCategory
    });
    
    var path = "http://img.dev.rappi.com/products/";
    for(var i=0;i<query.length;i++) {
          var products = [];
          for each (var item in query[i].field("products")) {
              var image = item.image;
              products.push({
                rid : item.rid,
                product_id : item.product_id,
                name : item.name,
                description : item.description,
                price : item.price,
                image : path + "low/" + image
              });
          }
      
          categories.push({
              category : query[i].field("category"),
                avg : query[i].field("avg"),
                image: pathImageCategory,
              products: products
          });
      
    }
    return categories;
  } catch ( err ){
    return {error: err.toString(), code: 400};
  }