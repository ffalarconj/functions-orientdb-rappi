var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var SecretToken = "uMHOhz6GpXHQGAQtg2tyxgD28UwqyLQd";

var winston = require('winston');

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      colorize: true,
      timestamp: true,
    })
  ],
  format: winston.format.combine(
      winston.format.colorize({ all: true }),
      winston.format.simple()
  )
});

/**
 * GET users listing.
 * This function comment is parsed by doctrine
 * @route GET /users
 * @group users - Operations about user
 * @param {string} email.query.required - username or email
 * @param {string} password.query.required - user's password.
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */
router.get('/', function(req, res, next) {
  res.send('respond with a resource: ' + res.__('hi'));
});

/**
 * POST login.
 * Function Login
 * @route POST /users/login
 * @group users - login
 * @param {User.model} Body.body.required - Username and Password
 * @consumes application/json
 * @produces application/json
 * @returns {ResponseToken.model} 200 - Token
 * @returns {Response401.model} 401 - Error user invalid
 * @returns {Error}  default - Unexpected error
 */

/**
 * @typedef User
 * @property {string} username.required
 * @property {string} password.required 
 */

/**
 * @typedef ResponseToken
 * @property {string} token.required
 */

/**
 * @typedef Response401
 * @property {string} error.required - usuario o contraseña inválidos
 */
router.post('/login', (req, res) => {
  var username = req.body.username
  var password = req.body.password

  logger.warn({username, password});
 
  if( !(username === 'freddy' && password === '1234')){
    res.status(401).send({
      error: res.__('user_password_invalid')
    })
    return
  }
 
  var tokenData = {
    username: username,
    secret: "CB3hIz53sUekCOEvcTpbGYGj2jHcFrDr",
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24), //expiresIn: 60 * 60 * 24 // expires in 24 hours
  };
 
  var token = jwt.sign(tokenData, SecretToken, {
     
  });
 
  res.send({
    token
  });

});

router.get('/secure', (req, res) => {
    var token = req.headers['authorization']
    if(!token){
        res.status(401).send({
          error: res.__('token_required')
        })
        return
    }

    token = token.replace('Bearer ', '')

    jwt.verify(token, SecretToken, function(err, user) {
      if (err) {
        res.status(401).send({
          error: err
        })
      } else {
        res.send({
          message: 'Awwwww yeah!!!!'
        })
      }
    })
})

module.exports = router;
