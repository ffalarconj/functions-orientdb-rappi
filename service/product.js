var db = require("../database/orientdb").db

exports.getProduct = () => {
    return new Promise((resolve, reject) => {
        try {
            db.open().then(() => {
                return db.record.get('#12:1')
                .then(function(record){
                    console.log('Loaded Record:', record);
                    db.close().then(() => {
                        console.log('closed');
                    });
                    resolve(record);
                });
            });
        } catch (err) {
            reject(err);
        }
    });
};

exports.getProducts = () => {
    return new Promise((resolve, reject) => {
        try {
            db.open().then(() => {
                return db.query('SELECT getSwipeCarousel()');
            }).then((results) => {
                db.close().then(() => {
                    console.log('closed');
                });
                resolve(results);
            });
        } catch (err) {
            reject(err);
        }
    });
};

exports.saveProduct = () => {
    return new Promise((resolve, reject) => {
        try {
            db.open().then(() => {
                return db.insert().into('Product')
                .set({
                  image : 'https://d25szaaiz1s57z.cloudfront.net/qbano/global/Qbano%20Nuevas%20Imag/hamburguesa.png',
                  name:  'Hamburguesa Doble',
                  price:  6000
                }).one().then(function(product){
                    db.close().then(() => {
                        console.log('closed');
                    });
                    resolve(product);
                });
            });
        } catch (err) {
            reject(err);
        }
    });
};

exports.updateProduct = () => {
    return new Promise((resolve, reject) => {
        try {
            db.open().then(() => {
                return db.update('#12:2')
                .set({
                  price: 7500
                }).one().then(function(product){
                    db.close().then(() => {
                        console.log('closed');
                    });
                    resolve(product);
                });
            });
        } catch (err) {
            reject(err);
        }
    });
};

exports.deleteProduct = () => {
    return new Promise((resolve, reject) => {
        try {
            db.open().then(() => {
                return db.delete().from('Product')
                .where('@rid = #12:0')
                .limit(1).scalar().then(function(del){
                    db.close().then(() => {
                        console.log('closed');
                    });
                    resolve(del);
                });
            });
        } catch (err) {
            reject(err);
        }
    });
};